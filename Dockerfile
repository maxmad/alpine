FROM alpine:latest

RUN addgroup -g 1000 -S alpine && \
    adduser -u 1000 -S alpine -G alpine

USER alpine

CMD ["sh"]
