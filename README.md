## Usage

```sh
docker run -it --rm \
 -e LOCAL_USER_ID=$(id -u $USER) \
 -e LOCAL_GROUP_ID=$(id -g $USER) \
 registry.gitlab.com/maxmad/alpine:latest sh
```