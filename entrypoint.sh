#!/bin/sh
set -e

# https://denibertovic.com/posts/handling-permissions-with-docker-volumes/
USER_ID=${LOCAL_USER_ID:-1000}
GROUP_ID=${LOCAL_GROUP_ID:-10001}

echo "Starting with UID : $USER_ID"
echo "Starting with GID : $GROUP_ID"
addgroup -S -g "$GROUP_ID" usergroup
adduser -S -D -s /bin/sh -h /home/user -u "$USER_ID" user -G usergroup
export HOME=/home/user

exec su-exec user "$@"
